#include <iostream>
#include <mutex>
#include <array>

int main()
{
    std::array stock = {122, 139, 115, 163, 138, 120, 68, 21, 61, 162, 119, 171, 164, 157, 123, 134, 102, 68, 157, 182, 31, 100, 76, 9, 115, 143, 78, 54, 152, 113, 61, 50, 139, 72, 14};
    int max = 0;
    int size = stock.size();
    std::mutex mutex;

    for (int i = 0; i < size; i++)
    {
        for (int j = i + 1; j < size; j++)
        {
            int dif = stock[j] - stock[i];
            std::cout << stock[j] << "-" << stock[i] << "=" << dif << std::endl;
            if (dif > max)
            {
                    max = dif;
            }
        }
    }

    std::cout << max << std::endl;

    return 0;
}