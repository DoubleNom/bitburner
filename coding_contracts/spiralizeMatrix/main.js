// source: https://www.geeksforgeeks.org/print-a-given-matrix-in-spiral-form/

function spiralPrint(res, m, n, arr) {
    let i, k = 0, l = 0;
    /*
        k - starting row index
        m - ending row index
        l - starting column index
        n - ending column index
        i - iterator 
    */
 
    while (k < m && l < n) {
        // print the first row from the remaining rows
        for (i = l; i < n; ++i) {
            res.push(arr[k][i]);
        }
        k++;
 
        // print the last column from the remaining columns
        for (i = k; i < m; ++i) {
            res.push(arr[i][n - 1]);
        }
        n--;
 
        // print the last row from the remaining rows
        if (k < m) {
            for (i = n - 1; i >= l; --i) {
                res.push(arr[m - 1][i]);
            }
            m--;
        }
 
        // print the first column from the remaining columns
        if (l < n) {
            for (i = m - 1; i >= k; --i) {
                res.push(arr[i][l]);
            }
            l++;
        }
    }
}
 
// function call
let arr = [
    [48,25,45,32,43,13,12,28,38,31,33,18, 8, 9,29],
    [44,14, 1,34, 7, 8, 6,13,29,36,41,37,23,12,33],
    [ 3, 1,11,17,50,29,27,23,17,50,28, 1, 5,11,20],
    [31,19,30,45,42,18, 3,14, 3,19,44, 1, 4,44,50],
    [48,46,37,18,27,18,43,44,28,45,12,40,12, 1,34],
    [28,20, 3,12,47,26,34,38,46,40, 2,20,43,28,31],
    [38,50,33,13,46,43,18,45,26,21,22,48,14, 6,48],
    [20,16,25,24,44,11,21,34,16,33, 9,43,11,14,38],
    [40, 6, 1,33,29,23,16, 8, 2,22, 2,39,10,42,34],
    [10,47,38,43,34, 3,21,13,44,24, 1,41,13,46,14],
    [37,41,49,36,17,44,46,11,38,33,20,44,30,48,20],
    [39, 1,32, 6,15,44,23,17,14,33,14,23,30,39,45],
    [40,24,13, 1,35,46,17,26,21,25,36,25, 2,46,25]
];
let r = arr.length;
let c = arr[0].length;
var res = [] 

spiralPrint(res, r, c, arr);

console.log(res.toString())
