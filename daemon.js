var scripts = [
    ["/utils/map_network.script", []],
    ["/utils/get_hacked_servers.script", []],
    ["/hack/remote_hack.script", []],
    ["/utils/buy_hacknet.js", []],
    ["/hack/init_hack.script", ["home", "nectar-net"]]
]

export async function main(ns) {
    scripts.forEach(function(script) {
        ns.print(script)
        ns.kill(script[0], "home")
        // if(!ns.scriptRunning("home", script))
            ns.exec(script[0], "home", 1, ...script[1])
    })
}